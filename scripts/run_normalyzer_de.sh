#!/usr/bin/env bash

echo "Usage: script de_settings.json"
echo "Processing..."

settings=${1}

Rscript /normalyzer/scripts/run_normalyzer_de.R ${settings} > /hostdata/log_de.out 2> /hostdata/log_de.err

echo "Differential expression finished, check output in hostdata directory"
