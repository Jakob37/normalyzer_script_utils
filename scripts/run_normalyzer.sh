#!/bin/bash

echo "Usage: script settings_json"
echo "Processing..."

settings=${1}

Rscript /normalyzer/scripts/run_normalyzer.R ${settings} > /hostdata/log_norm.out 2> /hostdata/log_norm.err

echo "Check output in hostdata directory"
